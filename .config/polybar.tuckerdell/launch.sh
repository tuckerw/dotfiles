#!/usr/bin/env sh

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -x polybar >/dev/null; do sleep 1; done

# Launch bar1 and bar2
# polybar example &

# for i in $(polybar -m | awk -F: '{print $1}'); do MONITOR=$i polybar example -c ~/.config/polybar/config & done
polybar top &

# if HMDI-1 monitor is plugged in generate polybar for it too
if polybar -m | grep -q "HDMI-1"; then
  polybar top_external &
fi

#redraw wallpaper:
# feh --bg-scale ~/.config/wall.png
# wal -c -i ~/.config/wall.png

# wal -R
# wal -a .5 -i ~/Pictures/walls/soldiers.jpg
# feh --bg-fill ~/Pictures/walls/icy_river.jpg

echo "Bars launched..."
