#!/usr/bin/env bash

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# Launch bar1 
echo "---" | tee -a /tmp/polybar1.log
echo "---" | tee -a /tmp/polybar2.log
polybar -q topbar1 >>/tmp/polybar1.log 2>&1 &
polybar topbar2 >>/tmp/polybar2.log 2>&1 &

echo "Bar launched..."
