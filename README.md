# How To Set Something Like This Up:

Create directory and initialize git repo
```
mkdir -p ~/repos/dotfiles
git init --bare $HOME/repos/dotfiles/
```

Add aliases to bash_aliases
```
alias dot='/usr/bin/git --git-dir=$HOME/repos/dotfiles/ --work-tree=$HOME'
alias dotfiles='/usr/bin/git --git-dir=$HOME/repos/dotfiles/ --work-tree=$HOME'
```

Set up .vimrc submodule
```
mv .vim .vim.bak
dot submodule add https://gitlab.com/tuckerw/vimrc.git ~/.vim
```

Don't show untracked files
```
dotfiles config --local status.showUntrackedFiles no
```

Add the origin
```
dot remote add origin https://gitlab.com/tuckerw/dotfiles.git
```

## Usage:

Set up for a new user/directory
```
mkdir -p ~/repos/dotfiles
cd !$
alias dot='git --git-dir=$HOME/repos/dotfiles/ --work-tree=$HOME'
dot clone --bare https://gitlab.com/tuckerw/dotfiles.git .
dot checkout -b origin/arch -ft
dot submodule update --init --recursive
```

To update the .vim submodule when changes are present:
```
dot submodule update --recursive .vim
```
