#alias keys="ssh-add /media/tucker/Keys/keys/tucker.id_rsa"
#alias bs="sshuttle --ssh-cmd 'ssh -i /media/tucker/Keys/keys/tucker.id_rsa -p 2222' -r twhitney@office.brownrice.com 0/0"

#####################
## SSH Connections ##
#####################
# alias keys="ssh-add /media/tucker/EnpassLin/keys/bastion.id_rsa; ssh-add ~/.ssh/id_rsa"
alias keys='eval `ssh-agent -s`; ssh-add /run/media/tucker/EnpassLin/keys/bastion.id_rsa; ssh-add ~/.ssh/id_rsa'
# alias bs="sshuttle --ssh-cmd 'ssh -i ~/.ssh/tucker.id_rsa -p 2222' -r twhitney@office.brownrice.com -x office.brownrice.com 0/0"
alias bbsocks="ssh -i /run/media/tucker/EnpassLin/keys/bastion.id_rsa -p 2222 twhitney@office.brownrice.com -N -D 1337"
alias bs="sshuttle -r twhitney@office.brownrice.com:2222 -x office.brownrice.com -x 192.168.0.0/24 0/0"
alias wj="ssh -J bri westaf" # see https://en.wikibooks.org/wiki/OpenSSH/Cookbook/Proxies_and_Jump_Hosts#Passing_Through_One_or_More_Gateways_Using_ProxyJump
alias bb="ssh -i /run/media/tucker/EnpassLin/keys/bastion.id_rsa -p 2222 twhitney@office.brownrice.com"
alias wb="ssh -i /run/media/tucker/EnpassLin/keys/bastion.id_rsa twhitney@westaf-bastion.westaf.org"
alias tn="ssh -i /run/media/tucker/EnpassLin/keys/bastion.id_rsa -p 2224 twhitney@bastion.taosnet.org"
alias rp="ssh -i ~/.ssh/pi.id_rsa pi@192.168.0.6"
alias braveprox="brave --proxy-server=socks://127.0.0.1:1337"

# alias bp="ssh -L 22222:bastion.westaf.org:22 twhitney@office.brownrice.com -p 2222"
# alias bb="ssh twhitney@office.brownrice.com -p 2222"
# alias wb="ssh twhitney@localhost -p 22222"

alias dt="xinput --disable 13"
alias et="xinput --enable 13"

# thesaurus:
alias thes='dict -d moby-thesaurus'

# sshmount /mnt/hersephoria to tucker@tuckerwhitney.com:/home/
alias herse="sudo sshfs tucker@tuckerwhitney.com:/home /mnt/hersephoria -o allow_other"
alias uherse="sudo umount -l /mnt/hersephoria"

## ssh to tuckerwhitney.com
alias tucker="ssh tucker@tuckerwhitney.com"
alias tmosh="mosh tucker@tuckerwhitney.com"

# youtube-dl
alias mp3="youtube-dl -x --audio-format mp3 --embed-thumbnail --add-metadata  -o '%(title)s.%(ext)s'"
alias best="youtube-dl -f bestaudio --embed-thumbnail --add-metadata -o '%(title)s.%(ext)s'"
alias vid="youtube-dl --add-metadata -o '%(title)s.%(ext)s'"

# tor
alias tor="sh -c '/home/tucker/bin/tor-browser_en-US/Browser/start-tor-browser --detach || ([ !  -x /home/tucker/bin/tor-browser_en-US/Browser/start-tor-browser ] && \"$(dirname \"$*\")\"/Browser/start-tor-browser --detach)' dummy %k"

# ls -althr
alias ll="ls -alh"
alias lr="ls -althr"
alias g="git"
alias sp="sudo pacman -S"

# git
alias g="git"
alias dot='/usr/bin/git --git-dir=$HOME/repos/dotfiles/ --work-tree=$HOME'
alias dotfiles='/usr/bin/git --git-dir=$HOME/repos/dotfiles/ --work-tree=$HOME'

# vim
alias v="vim"
alias vi="vim"
alias vmi="vim"
alias cim="vim"

# list aliases
alias al="cat ~/.bash_aliases"

# get crypto prices
alias crypto="curl -s https://rate.sx?q | awk '\$4 ~ /BTC|ETH|LTC/ {print \$4 \" \" \$6}'"
alias eth="curl -s https://rate.sx/eth?qTF | awk '\$1 ~ /avg:/ {print \$2}'"
alias ltc="curl -s https://rate.sx/ltc?qTF | awk '\$1 ~ /avg:/ {print \$2}'"
alias btc="curl -s https://rate.sx/btc?qTF | awk '\$1 ~ /avg:/ {print \$2}'"
alias xmr="curl -s https://rate.sx/xmr?qTF | awk '\$1 ~ /avg:/ {print \$2}'"
