#!/bin/bash

status=$(cmus-remote -Q);
title=$(echo "$status" | grep "tag title " | sed "s#^tag title ##g");
artist=$(echo "$status" | grep "tag artist " | sed "s#^tag artist ##g");
album=$(echo "$status" | grep "tag album " | sed "s#^tag album ##g");
playing=$(echo "$status" | egrep "^status " | sed "s#^status playing#▶#g; s#status paused#||#g; s#status stopped##g;");
date=$(echo "$status" | grep "tag date " | sed "s#^tag date ##g");
duration=$(echo "$status" | egrep "^duration|position" | tr '\n' ' ' | sed "s#^duration \|position ##g");
duration=$(echo "$duration" | awk '{print ($2 / 60)-(($2 % 60)*1/60) ":" ($2 % 60), "/", ($1 / 60)-(($1 % 60)*1/60) ":" ($1 % 60)}' | sed "s#:\([0-9]\)\( \|\$\)#:0\1\2#g#");
track=$(echo "$status" | grep "tag tracknumber" | sed "s#^tag tracknumber ##g");
echo "$playing $track - $title - $artist - $album | $duration | $date";

#     | grep '\(tag \b\(title\|artist\|album\)\b\)\|status' \
#     | sed 's#^\(status\|tag artist\|tag title\|tag album\) ##g;' \
#     | sed 's#^\(status\|tag artist\|tag title\|tag album\) ##g; s#\(^.\{,40\}\).*#\1#g' \
#     | tr '\n' '{' \
#     | sed 's/^playing{/▶ /g; s/^paused{/|| /g; s/^stopped/#/g; s#{$##g; s#{# - #g;'
#     # | awk '{print $1, $2, $3, $4, $5, $6, $7, $8, $9, $10}' | sed 's#\t# #g;' \
