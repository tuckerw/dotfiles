#!/bin/sh
find ~/Documents/Writing/ ~/Documents/Notes/ -name "*.md" -o -name "*.ms" -o -name "*.txt" -type f | dmenu -i -l 30 | xargs -I{} -r vim "{}"
