#!/bin/bash

# converts each filename argument (expects *.Rmd or *.rmd) into a pdf
for file in "$@"
do

  fname=$(echo "$file" | cut -f 1 -d '.')
  fext=$(echo "$file" | cut -f 2 -d '.')

  if [ "${fext,,}" = "rmd" ]; then
    # echo ".rmd"
    Rscript -e "library(rmarkdown); library(utils); render('$file', 'pdf_document')"
  elif [ "${fext,,}" = "md" ]; then
    # echo ".md"
    # without table of contents:
    # pandoc -V geometry:margin=1in -i --latex-engine=xelatex --variable mainfont="DejaVu Serif" --variable sansfont=Arial "$file" -o "$fname.pdf"
    # --toc : table of contents
    pandoc -V geometry:margin=1in -V block-headings -i --standalone --pdf-engine=xelatex --variable mainfont="DejaVu Serif" "$file" -o "$fname.pdf"
  elif [ "${fext,,}" = "ms" ]; then
    # echo ".ms"
    # use -rHY=0 for no hyphenation, -rHY=14 for mild hyphenation
    # groff -ms "$file" -rHY=12 -K utf-8 -T pdf > "$fname.pdf"
    groff -ms "$file" -rHY=12 -K utf-8 -T ps | ps2pdf - "$fname.pdf"
    # sed 's/ \?-- \?/—/g' "$file" | groff -ms -T pdf > "$fname.pdf"
  elif [ "${fext,,}" = "mom" ]; then
    groff -mom "$file" -K utf-8 -T pdf > "$fname.pdf"
  else
    echo "Invalid Filetype: $fext"
  fi

done
