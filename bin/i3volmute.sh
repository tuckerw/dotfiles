#!/bin/sh
SINK="$(pacmd list-sinks | awk '/\* index\:/ {print $3}')"
pactl set-sink-mute ${SINK} toggle #mute volume
