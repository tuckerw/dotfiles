#!/bin/sh
find ~/Documents/Writing/ ~/Documents/Notes/ -name "*.pdf" | dmenu -i -l 30 | xargs -I{} -r zathura "{}"
