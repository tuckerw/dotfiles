#!/bin/sh

CMUS="$(cmus-remote -Q)"
STAT="$(cmus-remote -Q | sed 1q | awk '{print $2}')"

case $BLOCK_BUTTON in
  1) if [ "$STAT" = "playing" ]
  then
    cmus-remote -u 
  else
    cmus-remote -p
  fi ;;
  3) notify-send "${CMUS}" ;;
esac

echo -n "${CMUS}" | awk '/set vol_left / {printf $3 "%   "}'

case "$STAT" in
  "stopped" )
    echo -n "  " ;;
  "playing" )
    echo -n "  " ;;
  "paused" )
    echo -n "  " ;;
esac

echo "${CMUS}" | awk '/tag (artist|album|title) /' | cut -d\  -f3- | awk '// {print}' ORS='  ' | sed 's/.\{3\}$//'
