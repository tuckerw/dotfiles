#!/bin/sh

case $BLOCK_BUTTON in
  1) notify-send "$(calcurse -d 365 | head -n 20)" ;;
  2) notify-send "$(date '+%H:%M' | figlet)" ;;
  3) notify-send "$(calcurse -t | awk '/[0-9]\./' | cut -d' ' -f 2-)" ;;
esac

date '+%Y-%m-%d %H:%M:%S'
