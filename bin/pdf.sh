#!/bin/sh
find ~/Music ~/Documents/ -name "*.pdf" -type f | dmenu -i -l 10 | xargs -I{} -r zathura "{}"
