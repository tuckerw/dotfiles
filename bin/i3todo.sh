#!/bin/sh

case $BLOCK_BUTTON in
  1) notify-send "$(sed '/Above this line/Q' ~/Documents/Notes/Tasks.md)" ;;
  #3) notify-send "$(calcurse -t | awk '/[0-9]\./' | cut -d' ' -f 2-)" ;;
esac

sed '/Above this line/Q' ~/Documents/Notes/Tasks.md | awk '/^* /' | wc -l
