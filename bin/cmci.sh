#!/bin/bash

find "$(cmus-remote -C status | grep file | sed 's#file ##g;' | xargs -0 dirname)" \
    | grep "jpg\|png" \
    | tr "\n" "\0" \
    | xargs -0 -I{} feh --scale-down "{}"

