#!/bin/sh

case $BLOCK_BUTTON in
  1) notify-send "Memory Hogs:
$(ps axch -o pid:7,cmd:20,%mem --sort=-%mem | head)" ;;
  #3) right-click action
esac

free -h | awk '/^Mem:/ {print $3 "/" $2}'
