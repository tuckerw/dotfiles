#!/bin/sh

# To Do: Merge this and gms.sh, so it auto-detects extension and then does appropriate conversion command
# Then make them just one keyboard mapping: `bc` in ranger (rc.conf)

# converts each filename argument (expects *.Rmd or *.rmd) into a pdf
for fname in "$@"
do
  Rscript -e "library(rmarkdown); library(utils); render('$fname', 'pdf_document')"
done
