#!/bin/sh
find /home/tucker/Documents/Videos/ -type f | dmenu -i -l 30 | tr -d '\n' | xargs -0 -r -I{} mpv "{}"
