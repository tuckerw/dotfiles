#!/bin/sh

BATT="$(acpi | cut -d ' ' -f3-5)"
BATT1="$(echo ${BATT} | cut -d' ' -f1 | sed 's/,//')"
BATT2="$(echo ${BATT} | cut -d' ' -f2 | sed 's/%,\?//')"
BATT3="$(echo ${BATT} | cut -d' ' -f3 | sed 's/%,//;s/^0*://g')"

# acpi | cut -d " " -f3-5 | sed -e "s/,//g;s/Discharging//;s/Charging//;s/Unknown/?/;s/Full//;s/ 0*/ /g;s/ :/ /g"

if [ "$BATT1" = "Charging" -o "$BATT1" = "Full" ] ; then
  CHG=" "
else
  CHG=""
fi

if [ "$BATT2" -gt 95 ] ; then
  # full
  ICON=""
elif [ "$BATT2" -gt 63 ] ; then
  # 3/4
  ICON=""
elif [ "$BATT2" -gt 37 ] ; then
  # half
  ICON=""
elif [ "$BATT2" -gt 12 ] ; then
  # 1/4
  ICON=""
else
  # empty
  ICON=""
fi

echo "${CHG} ${ICON}  ${BATT2}% ${BATT3}"
