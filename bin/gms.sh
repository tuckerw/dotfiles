#!/bin/sh

for fname in "$@"
do
  ffname=$(echo "$fname" | cut -f 1 -d '.')
  groff -ms "$fname" -T pdf > "$ffname.pdf"
done
