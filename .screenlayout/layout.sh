#!/bin/sh
xrandr --output DisplayPort-3 --off --output DisplayPort-4 --off --output DisplayPort-5 --off --output HDMI-A-3 --mode 2560x1080 --pos 0x0 --rotate normal --output DVI-D-0 --off --output VGA-1-1 --off --output HDMI-1-1 --off --output DP-1-1 --off --output HDMI-1-2 --off --output HDMI-1-3 --off --output DP-1-2 --off --output DP-1-3 --off
